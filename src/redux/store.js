import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from './root';

const logger = createLogger();

export default applyMiddleware(thunk, logger)(createStore)(rootReducer);
