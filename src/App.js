import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Footer from './Components/Footer';
import Home from "./Pages/Home";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Caarya Template</h1>
      </header>
      <main>
        <Router>
          <Switch>
            <Route exact path="/" render={() => <Home />} />
          </Switch>
        </Router>
      </main>
      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default App;
